#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "TargetCapailitiesTopLevel.txt"

char szBuffer[1024];

void main()
{
  FILE *f;

  f = fopen(FILENAME,"r");

  while (!feof(f))
    {
      fgets(szBuffer,sizeof(szBuffer),f);
      if ( szBuffer[strlen(szBuffer)-1] <32 )
	szBuffer[strlen(szBuffer)-1] = '\0';
      if ( szBuffer[0]>' ' )
	{
	  printf("      </itemizedlist>\n");
	  printf("    </para>\n");
	  printf("  </section>\n");
	  printf("  <section>\n");
	  printf("    <title>%s</title>\n",szBuffer);
	  printf("    <para>\n");
	  printf("      <simplelist>\n");
	}
      if ( !strncmp(" - ",szBuffer,3) )
	printf("      <member>%s</member>\n",&szBuffer[3]);
      if ( !strncmp("  ",szBuffer,2) )
	{
	  printf("      <listentry>\n");
	  printf("        <para>\n");
	  printf("          %s\n",&szBuffer[2]);
	  printf("        </para>\n");
	  printf("      </listentry>\n");
	}
    }
}
